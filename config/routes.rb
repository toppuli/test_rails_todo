Rails.application.routes.draw do
  resources :todo_lists
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'todo_lists/index'
  root 'todo_lists#index'
end
